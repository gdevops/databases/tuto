
.. index::
   pair: dbping ; Sybase
   pair: docker ; exec
   pair: docker ; run

.. _dbping:

==================================================================
sybase dbping (**ping utility**)
==================================================================

.. seealso::

   - http://infocenter.sybase.com/help/index.jsp?topic=/com.sybase.infocenter.dc00168.1510/html/iqutil/CHDHIHAH.htm





Documentation officielle
==========================

.. seealso::

   - http://infocenter.sybase.com/help/index.jsp?topic=/com.sybase.infocenter.dc00168.1510/html/iqutil/CHDHIHAH.htm


.. warning:: la documentation officielle date de 2009 !.


Script test_accesseng_database.bat
=====================================

::

	%echo on

	REM base distante "eng_database.db_database"
	dbping -c "links=tcpip(host=X.Y.Z.T:2638);ENG=eng_database;DBN=db_database;UID=gpao;PWD=flat" -d -z



Utilisation à partir de l'image Docker
----------------------------------------

.. seealso::

   - https://github.com/jaswdr/docker-image-sybase

::

    docker run -ti test_sqlanywhere:latest bash


ou::

    docker run -d -p 8000:5000 -p 8001:5001 --name myany test_sqlanywhere:latest
    docker exec -ti myany bash


::

    dbping -c "links=tcpip(host=X.Y.Z.T:2638);ENG=eng_database;DBN=db_database;UID=gpao;PWD=flat" -d -z


::

	SQL Anywhere - Utilitaire Ping Version 10.0.1.4310
	Tentative de connexion avec :
	UID=gpao;PWD=********;DBN=db_database;ENG=eng_database;LINKS='tcpip(host=X.Y.Z.T:2638)'
	Tentative de connexion à un serveur en cours d'exécution...
	Essai de création du lien TCPIP...
	TCP utilisant Winsock version 2.2
	Mon adresse IP est 172.24.96.1
	Mon adresse IP est 10.0.40.41
	Mon adresse IP est ::1
	Mon adresse IP est 127.0.0.1
		Lien TCPIP démarré correctement
	Tentative de connexion TCPIP (adresse X.Y.Z.T:2638 trouvée dans le cache sasrv.ini)
	Tentative de recherche du serveur à l'adresse mise en cache X.Y.Z.T:2638 sans diffusion
	Serveur trouvé, vérification du nom du serveur
	Serveur de base de données trouvé à l'adresse X.Y.Z.T
	Serveur eng_database trouvé sur le lien TCPIP
	Connecté à l'aide de l'adresse du client 10.0.40.41:60875
	Connexion au serveur via TCPIP, à l'adresse X.Y.Z.T:2638
	Connecté au serveur SQL Anywhere version 10.0.1.3974
	La langue 'french' n'est pas supportée, la langue 'us_english' sera utilisée à la place
	Informations sur l'application :
	IP=172.24.96.1;HOST=UC026;OS='Windows Vista Build 9200 ';PID=0x25a8;THREAD=0x2764;EXE='C:\Program Files\SQL Anywhere 10\x64\dbping.exe';VERSION=10.0.1.4310;API=DBLIB;TIMEZONEADJUSTMENT=120
	Connecté au serveur, tentative de connexion à une base de données active...
	Connecté à la base de données avec un avertissement
	Connexion établie au serveur SQL Anywhere 10.0.1.3974 "eng_database" et à la base de données "db_database" à l'adresse X.Y.Z.T.
	Client déconnecté
	Ping de la base de données réussi.
