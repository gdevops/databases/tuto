.. Databases tutorial documentation master file, created by
   sphinx-quickstart on Wed Mar 21 16:27:34 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://gdevops.frama.io/databases/tuto/rss.xml>`_

.. _databases_tutorial:
.. _tuto_databases:

=========================
**Databases** tutorial
=========================

- https://postgresweekly.com


.. toctree::
   :maxdepth: 3
   :caption: Databases

   sql/sql
   people/people
   mariadb/mariadb
   mysql/mysql
   postgresql/postgresql
   sqlite/sqlite
   other/other
   tools/tools
