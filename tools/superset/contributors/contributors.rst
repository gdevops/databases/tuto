
.. _superset_contributors:

==============================================================================
Apache **superset** community
==============================================================================

- https://superset.apache.org/community
- https://github.com/apache-superset/awesome-apache-superset
- https://www.meetup.com/en-AU/Global-Apache-Superset-Community-Meetup/


Maxime Beauchemin
==================

- https://github.com/mistercrunch

creator of Apache Airflow and Apache Superset - founder at Preset.


Elizabeth Thompson
=====================

- https://github.com/eschutho


Beto Dealmeida
================

- https://blog.taoetc.org/
- https://2c.taoetc.org/@beto
