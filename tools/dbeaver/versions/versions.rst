.. index::
   pair: DBeaver ; Versions

.. _dbeaver_versions:

======================
DBeaver **versions**
======================

.. seealso::

   - https://github.com/dbeaver/dbeaver

.. toctree::
   :maxdepth: 3

   7.0.4/7.0.4
   7.0.0/7.0.0
