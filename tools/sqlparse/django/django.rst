.. index::
   pair: Django ; sqlparse
   ! sqlparse

.. _sqlparse_django:

==============================================================
**sqlparse** and Django
==============================================================

.. seealso::

   - https://github.com/andialbrecht/sqlparse




Example 1
==========


::

    import sqlparse
    sql_query = sqlparse.format(str(liste.query), reindent=True)
    logger.info(f"\n{current_day=}\nSQL query:\n{sql_query}\n")
