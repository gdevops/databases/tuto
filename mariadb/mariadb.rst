.. index::
   ! MariaDB

.. _mariadb:

===========
MariaDB
===========

- https://en.wikipedia.org/wiki/MariaDB
- https://mariadb.org/
- https://github.com/MariaDB/server
- https://hub.docker.com/_/mariadb


.. toctree::
   :maxdepth: 3


   installation/installation
   service/service
   commands/commands
   docker/docker
   faq/faq
   versions/versions
