
.. index::
   pair: Redis ; Definition

.. _redis_dev:

===================
Redis definition
===================




Antirez definition
===================

   - https://github.com/antirez/redis

Redis is an in-memory database that persists on disk.

The data model is key-value, but many different kind of values are supported:
Strings, Lists, Sets, Sorted Sets, Hashes, HyperLogLogs, Bitmaps

English wikipedia definition
============================

.. seealso::

   - https://en.wikipedia.org/wiki/Redis


Redis (/ˈrɛdɪs/;  Remote Dictionary Server)[5] is an in-memory data structure
project implementing a distributed, in-memory key-value database with optional
durability.

Redis supports different kinds of abstract data structures, such as strings,
lists, maps, sets, sorted sets, HyperLogLogs, bitmaps, streams, and spatial
indexes.

The project is mainly developed by Salvatore Sanfilippo and as of 2019, is
sponsored by Redis Labs.

It is open-source software released under a BSD 3-clause license.

French wikipedia definition
============================

.. seealso::

   - https://fr.wikipedia.org/wiki/Redis


Redis (de l'anglais REmote DIctionary Server qui peut être traduit par
« serveur de dictionnaire distant » et jeu de mot avec Redistribute) est
un système de gestion de base de données clef-valeur scalable, très hautes
performances, écrit en C ANSI et distribué sous licence BSD.

Il fait partie de la mouvance NoSQL et vise à fournir les performances les
plus élevées possibles.
