
.. index::
   ! Sybase central


.. _sybase_central:

======================================================
``sybase central`` to access SQL Anywhere databases
======================================================


.. seealso::

   - :ref:`sqlanywhere`
   - http://fadace.developpez.com/asa/v10/


.. figure:: sybase_central.png
   :align: center



.. warning:: Note that there are Sybase ASE and Sybase SQL Anywhere systems.
   Those two are completely different database engines.
