
.. index::
   pair: SQL Anywhere ; Versions
   pair: SQL Anywhere ; 10.0 (2006)
   pair: SQL Anywhere ; 11 (2008)
   pair: SQL Anywhere ; 12.0 (2010)
   pair: SQL Anywhere ; 16.0 (2013)
   pair: SQL Anywhere ; 17.0 (2015)

.. _versions_sqlanywhere:

=================================
Versions SQL Anywhere
=================================

.. seealso::

   - https://en.wikipedia.org/wiki/SQL_Anywhere
   - http://sqlanywhere-forum.sap.com/

.. toctree::
   :maxdepth: 4


.. figure:: history.png
   :align: center




.. _sqlanywhere_17:

SQL Anywhere 17.0 (2015)
===========================

.. seealso::

   - https://blogs.sap.com/2015/07/15/announcing-sql-anywhere-17-2/


.. _sqlanywhere_16:

SQL Anywhere 16.0 (2013)
===========================

.. seealso::

   - https://github.com/jaswdr/docker-image-sybase



.. _sqlanywhere_12:

SQL Anywhere 12.0 (2010)
===========================




.. _sqlanywhere_11:

SQL Anywhere 11.0 (2008)
===========================



.. _sqlanywhere_10:

SQL Anywhere 10.0 (2006)
===========================
