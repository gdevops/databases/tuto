.. index::
   pair: superset ; Tool
   ! superset



.. figure:: images/apache.png
   :align: center


.. figure:: images/apache_superset.png
   :align: center

.. _superset:

==============================================================================
Apache **superset** is a Data Visualization and Data Exploration Platform
==============================================================================

- https://gitbox.apache.org/repos/asf?p=superset.git
- https://github.com/apache/superset
- https://github.com/apache/superset/graphs/contributors
- https://superset.apache.org/
- https://projects.apache.org/committee.html?superset
- https://superset.apache.org/gallery

Was used to be named Caravel, and Panoramix in the past.


.. figure:: images/apache_superset_project.png
   :align: center

   https://projects.apache.org/committee.html?superset


.. toctree::
   :maxdepth: 3

   overview/overview
   contributors/contributors
   articles/articles
   installation/installation
   roadmap/roadmap
   used_by/used_by
   integrations/integrations
   versions/versions
