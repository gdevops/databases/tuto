
.. index::
   pair: MariaDB ; Versions

.. _mariadb_versions:

=================
MariaDB versions
=================

.. seealso::

   - https://github.com/MariaDB/server/releases
   - https://mariadb.com/kb/en/release-notes/
   - https://mariadb.com/kb/en/changelogs/


.. toctree::
   :maxdepth: 3


   10.6/10.6
   10.5/10.5
   10.4/10.4
   10.3/10.3
   5.5/5.5
