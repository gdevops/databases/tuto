.. index::
   pair: superset ; installation

.. _superset_installation:

====================================================================
**superset** installation
====================================================================


.. toctree::
   :maxdepth: 3

   venv/venv
   docker_compose/docker_compose
