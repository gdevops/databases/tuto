.. index::
   pair: superset ; Versions

.. _superset_versions:

====================================================================
**superset** versions
====================================================================

- https://gitbox.apache.org/repos/asf?p=superset.git
- https://github.com/apache/superset/releases
- https://github.com/apache/superset/graphs/contributors

.. toctree::
   :maxdepth: 3

   2.1.0/2.1.0
   2.0.0/2.0.0
   1.4.0/1.4.0
   1.3.0/1.3.0
   1.2.0/1.2.0
