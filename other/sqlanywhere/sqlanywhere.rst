
.. index::
   pair: Système de gestion de Base de données; sqlanywhere
   pair: Migration de données ; sqlanywhere
   pair: SQL Anywhere ; sqlanywhere
   ! sqlanywhere

.. _sqlanywhere:

==================================================================================
Le système de gestion de bases de données relationnelles  SQL Anywhere
==================================================================================

.. seealso::

   - https://en.wikipedia.org/wiki/SQL_Anywhere
   - http://sqlanywhere-forum.sap.com/
   - http://docs.sqlalchemy.org/en/latest/dialects/sybase.html
   - https://wiki.python.org/moin/Sybase
   - https://hynek.me/articles/a-short-summary-on-sybase-sql-anywhere-python/
   - https://hynek.me/articles/celery-and-sybase/


.. toctree::
   :maxdepth: 4



sqlanywhere
=============

SAP SQL Anywhere is a proprietary relational database management system
(RDBMS) product from SAP.

SQL Anywhere was known as Sybase SQL Anywhere prior to the acquisition
of Sybase by SAP.


.. warning:: Note that there are Sybase ASE and SAP SQL Anywhere systems.
   Those two are completely different database engines.


L'outil graphique Sybase central utilise **SQL Anywhere**.

Sybase ASE
-----------

- https://en.wikipedia.org/wiki/Adaptive_Server_Enterprise

Forum
=====

- http://sqlanywhere-forum.sap.com/


OS Implémentations
==================

.. toctree::
   :maxdepth: 3


   os/os


Outils sqlanywhere
====================


.. toctree::
   :maxdepth: 3

   tools/tools


SQL Anywhere versions
=======================

.. toctree::
   :maxdepth: 3


   versions/versions
