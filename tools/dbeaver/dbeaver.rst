
.. index::
   pair: Tool ; DBeaver
   ! DBeaver

.. _dbeaver:

=========================================================
**DBeaver** (Free universal database tool and SQL client)
=========================================================

.. seealso::

   - https://github.com/dbeaver/dbeaver
   - https://dbeaver.io/
   - https://x.com/dbeaver_news


.. toctree::
   :maxdepth: 3

   description/description
   examples/examples
   versions/versions
