.. index::
   pair: MariaDB ; Installation
   
.. _mariadb_installation:

=====================
MariaDB installation
=====================

.. toctree::
   :maxdepth: 3

   timezones/timezones
   debian/debian
   wsl_ubuntu/wsl_ubuntu
