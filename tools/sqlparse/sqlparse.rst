.. index::
   pair: Tool ; sqlparse
   ! sqlparse

.. _sqlparse:

==============================================================
**sqlparse** A non-validating SQL parser module for Python
==============================================================

.. seealso::

   - https://github.com/andialbrecht/sqlparse

.. toctree::
   :maxdepth: 3

   django/django
