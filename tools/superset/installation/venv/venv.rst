
.. _superset_installation_venv:

====================================================================
**superset** installation
====================================================================

- https://superset.apache.org/docs/installation/installing-superset-from-scratch

::

    ~/experimentation/2021
    ✦ ❯ mkdir superset2

    ~/experimentation/2021
    ✦ ❯ cd superset2

    experimentation/2021/superset2
    ✦2 ❯ python -m venv venv

    experimentation/2021/superset2 took 2s
    ✦ ❯ . venv/bin/activate

    experimentation/2021/superset2 via 🐍 v3.10.0 (venv)
    ✦ ❯ pip install apache-superset
    Collecting apache-superset
      Downloading apache-superset-1.3.2.tar.gz (
