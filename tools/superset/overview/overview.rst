
.. _superset_overview:

============
Overview
============


What is Apache Superset ?
==============================

- https://superset.apache.org/docs/intro

Apache Superset is a modern, enterprise-ready business intelligence web
application. It is fast, lightweight, intuitive, and loaded with options
that make it easy for users of all skill sets to explore and visualize
their data, from simple pie charts to highly detailed deck.gl geospatial charts.


Superset provides
==================

- An intuitive interface for visualizing datasets and crafting interactive dashboards
- A wide array of beautiful visualizations to showcase your data
- Code-free visualization builder to extract and present datasets
- A world-class SQL IDE for preparing data for visualization, including
  a rich metadata browser
- A lightweight semantic layer which empowers data analysts to quickly
  define custom dimensions and metrics
- Out-of-the-box support for most SQL-speaking databases
- Seamless, in-memory asynchronous caching and queries
- An extensible security model that allows configuration of very intricate
  rules on on who can access which product features and datasets.
- Integration with major authentication backends (database, OpenID,
  LDAP, OAuth, REMOTE_USER, etc)
- The ability to add custom visualization plugins
- An API for programmatic customization
- A cloud-native architecture designed from the ground up for scale



Powerful yet easy to use
==========================


Quickly and easily integrate and explore your data, using either our
simple no-code viz builder or state of the art SQL IDE.


Integrates with modern databases
==================================

Superset can connect to any SQL based datasource through SQLAlchemy,
including modern cloud native databases and engines at petabyte scale.


Modern architecture
=======================

Superset is lightweight and highly scalable, leveraging the power of
your existing data infrastructure without requiring yet another ingestion
layer.


Rich visualizations and dashboards
====================================

Superset ships with a wide array of beautiful visualizations.

Our visualization plug-in architecture makes it easy to build custom
visualizations that drop directly into Superset.
