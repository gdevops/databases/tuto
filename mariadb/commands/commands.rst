
.. index::
   pair: Mariadb ; commands


.. _mariadb_commands:

===================
Mariabd commands
===================


.. seealso::

   - https://en.wikipedia.org/wiki/MariaDB


.. toctree::
   :maxdepth: 3

   mysql/mysql
   mysqldump/mysqldump
