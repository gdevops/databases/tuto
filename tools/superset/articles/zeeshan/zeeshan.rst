
.. _zeeshan_superset_articles:

========================================================
**Apache Superset With Postgresql**
========================================================

- https://zeeshan-tariq.medium.com/
- https://zeeshan-tariq.medium.com/apache-superset-with-postgresql-e7c08ff41c19

SUPERSET_LOAD_EXAMPLES
=======================

One important variable is SUPERSET_LOAD_EXAMPLES which determines whether
the superset_init container will load example data and visualizations
into the database and Superset.

These examples are quite helpful for most people, but probably unnecessary
for experienced users.

The loading process can sometimes take a few minutes and a good amount
of CPU, so you may want to disable it on a resource-constrained device


Log in to Superset
====================

Your local Superset instance also includes a Postgres server to store
your data and is already pre-loaded with some example datasets that ship
with Superset.

You can access Superset now via your web browser by visiting http://localhost:8088
