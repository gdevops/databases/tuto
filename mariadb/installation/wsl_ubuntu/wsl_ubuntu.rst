

.. index::
   pair: MariaDB ; WSL Ubuntu


.. _mariadb_wsl:

=====================================
MariaDB installation on WSL Ubuntu
=====================================






sudo apt install mariadb-server
=================================


::

    sudo apt install mariadb-server


::

	Lecture des listes de paquets... Fait
	Construction de l'arbre des dépendances
	Lecture des informations d'état... Fait
	Les paquets suivants ont été installés automatiquement et ne sont plus nécessaires :
	  ieee-data python-crypto python-ecdsa python-httplib2 python-jinja2 python-markupsafe python-netaddr
	  python-paramiko python-pkg-resources python-selinux python-yaml
	Veuillez utiliser « sudo apt autoremove » pour les supprimer.
	Les paquets supplémentaires suivants vont être installés :
	  libaio1 libcgi-fast-perl libcgi-pm-perl libdbd-mysql-perl libdbi-perl libencode-locale-perl libfcgi-perl
	  libhtml-parser-perl libhtml-tagset-perl libhtml-template-perl libhttp-date-perl libhttp-message-perl libio-html-perl
	  liblwp-mediatypes-perl libmysqlclient20 libterm-readkey-perl liburi-perl mariadb-client-10.0 mariadb-client-core-10.0
	  mariadb-common mariadb-server-10.0 mariadb-server-core-10.0 mysql-common
	Paquets suggérés :
	  libclone-perl libmldbm-perl libnet-daemon-perl libsql-statement-perl libdata-dump-perl libipc-sharedcache-perl libwww-perl mailx mariadb-test tinyca
	Les NOUVEAUX paquets suivants seront installés :
	  libaio1 libcgi-fast-perl libcgi-pm-perl libdbd-mysql-perl libdbi-perl libencode-locale-perl libfcgi-perl libhtml-parser-perl
	  libhtml-tagset-perl libhtml-template-perl libhttp-date-perl libhttp-message-perl libio-html-perl
	  liblwp-mediatypes-perl libmysqlclient20 libterm-readkey-perl liburi-perl mariadb-client-10.0 mariadb-client-core-10.0
	  mariadb-common mariadb-server mariadb-server-10.0 mariadb-server-core-10.0 mysql-common
	0 mis à jour, 24 nouvellement installés, 0 à enlever et 34 non mis à jour.
	Il est nécessaire de prendre 16,9 Mo dans les archives.
	Après cette opération, 147 Mo d'espace disque supplémentaires seront utilisés.
	Souhaitez-vous continuer ? [O/n] O
	Réception de:1 http://archive.ubuntu.com/ubuntu xenial-updates/main amd64 mysql-common all 5.7.21-0ubuntu0.16.04.1 [15,7 kB]
	Réception de:2 http://archive.ubuntu.com/ubuntu xenial-updates/universe amd64 mariadb-common all 10.0.34-0ubuntu0.16.04.1 [16,0 kB]
	Réception de:3 http://archive.ubuntu.com/ubuntu xenial/main amd64 libdbi-perl amd64 1.634-1build1 [743 kB]
	Réception de:4 http://archive.ubuntu.com/ubuntu xenial-updates/main amd64 libmysqlclient20 amd64 5.7.21-0ubuntu0.16.04.1 [809 kB]
	Réception de:5 http://archive.ubuntu.com/ubuntu xenial-updates/universe amd64 libdbd-mysql-perl amd64 4.033-1ubuntu0.1 [84,3 kB]
	Réception de:6 http://archive.ubuntu.com/ubuntu xenial/universe amd64 libterm-readkey-perl amd64 2.33-1build1 [27,2 kB]
	Réception de:7 http://archive.ubuntu.com/ubuntu xenial/main amd64 libaio1 amd64 0.3.110-2 [6 356 B]
	Réception de:8 http://archive.ubuntu.com/ubuntu xenial-updates/universe amd64 mariadb-client-core-10.0 amd64 10.0.34-0ubuntu0.16.04.1 [4 289 kB]
	Réception de:9 http://archive.ubuntu.com/ubuntu xenial-updates/universe amd64 mariadb-client-10.0 amd64 10.0.34-0ubuntu0.16.04.1 [1 173 kB]
	Réception de:10 http://archive.ubuntu.com/ubuntu xenial-updates/universe amd64 mariadb-server-core-10.0 amd64 10.0.34-0ubuntu0.16.04.1 [4 851 kB]
	Réception de:11 http://archive.ubuntu.com/ubuntu xenial-updates/universe amd64 mariadb-server-10.0 amd64 10.0.34-0ubuntu0.16.04.1 [4 268 kB]
	Réception de:12 http://archive.ubuntu.com/ubuntu xenial/main amd64 libhtml-tagset-perl all 3.20-2 [13,5 kB]
	Réception de:13 http://archive.ubuntu.com/ubuntu xenial/main amd64 liburi-perl all 1.71-1 [76,9 kB]
	Réception de:14 http://archive.ubuntu.com/ubuntu xenial/main amd64 libhtml-parser-perl amd64 3.72-1 [86,1 kB]
	Réception de:15 http://archive.ubuntu.com/ubuntu xenial/main amd64 libcgi-pm-perl all 4.26-1 [185 kB]
	Réception de:16 http://archive.ubuntu.com/ubuntu xenial/main amd64 libfcgi-perl amd64 0.77-1build1 [32,3 kB]
	Réception de:17 http://archive.ubuntu.com/ubuntu xenial/main amd64 libcgi-fast-perl all 1:2.10-1 [10,2 kB]
	Réception de:18 http://archive.ubuntu.com/ubuntu xenial/main amd64 libencode-locale-perl all 1.05-1 [12,3 kB]
	Réception de:19 http://archive.ubuntu.com/ubuntu xenial/main amd64 libhtml-template-perl all 2.95-2 [60,4 kB]
	Réception de:20 http://archive.ubuntu.com/ubuntu xenial/main amd64 libhttp-date-perl all 6.02-1 [10,4 kB]
	Réception de:21 http://archive.ubuntu.com/ubuntu xenial/main amd64 libio-html-perl all 1.001-1 [14,9 kB]
	Réception de:22 http://archive.ubuntu.com/ubuntu xenial/main amd64 liblwp-mediatypes-perl all 6.02-1 [21,7 kB]
	Réception de:23 http://archive.ubuntu.com/ubuntu xenial/main amd64 libhttp-message-perl all 6.11-1 [74,3 kB]
	Réception de:24 http://archive.ubuntu.com/ubuntu xenial-updates/universe amd64 mariadb-server all 10.0.34-0ubuntu0.16.04.1 [13,1 kB]
	16,9 Mo réceptionnés en 30s (558 ko/s)
	Préconfiguration des paquets...
	Sélection du paquet mysql-common précédemment désélectionné.
	(Lecture de la base de données... 40689 fichiers et répertoires déjà installés.)
	Préparation du dépaquetage de .../mysql-common_5.7.21-0ubuntu0.16.04.1_all.deb ...
	Dépaquetage de mysql-common (5.7.21-0ubuntu0.16.04.1) ...
	Sélection du paquet mariadb-common précédemment désélectionné.
	Préparation du dépaquetage de .../mariadb-common_10.0.34-0ubuntu0.16.04.1_all.deb ...
	Dépaquetage de mariadb-common (10.0.34-0ubuntu0.16.04.1) ...
	Sélection du paquet libdbi-perl précédemment désélectionné.
	Préparation du dépaquetage de .../libdbi-perl_1.634-1build1_amd64.deb ...
	Dépaquetage de libdbi-perl (1.634-1build1) ...

    ...




sudo service mysql start
==========================


::

    $ sudo service mysql start


::

   * Starting MariaDB database server mysqld
