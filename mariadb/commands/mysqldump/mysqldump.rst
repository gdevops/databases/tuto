
.. index::
   pair: Mariadb ; mysqldump


.. _mysqldump:

===================
mysqldump
===================


.. seealso::

   - https://en.wikipedia.org/wiki/MariaDB



mysqldump --help
==================

.. literalinclude:: mysqldump.txt
   :linenos:
