
.. index::
   pair: Database ; MySQL


.. _mysql:

===========
MySQL
===========


.. seealso::

   - https://en.wikipedia.org/wiki/MySQL


.. toctree::
   :maxdepth: 3


   versions/versions
