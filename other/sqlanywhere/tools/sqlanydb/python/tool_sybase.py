"""tool_sybase.py

.. seealso::

   - http://infocenter.sybase.com/help/index.jsp?topic=/com.sybase.help.sqlanywhere.12.0.1/dbprogramming/pg-python.html

"""

import logging

from typing import List

# https://docs.python.org/3.7/library/collections.html?highlight=namedtuple#collections.namedtuple
from collections import namedtuple

# https://github.com/sqlanywhere/sqlanydb
import sqlanydb

PARAMS_DBGPAO = {
    "links": "tcpip(host=192.168.202.254:2638)",
    "eng": "dbgpao",
    "dbn": "id3semi",
    "uid": "gpao",
    "pwd": "flat",
}

PARAMS_DBINTRANET = {
    "links": "tcpip(host=192.168.202.254:2639)",
    "eng": "dbintranet",
    "dbn": "intranet",
    "uid": "id3dba",
    "pwd": "root",
}


# Get an instance of a logger
logger = logging.getLogger(__name__)


__all__ = ["iter_sybase_table", "print_sybase_records", "get_sybase_records"]


def varchar_callback(valueToConvert):
    """"""
    print(f"varchar_callback")

    try:
        print(valueToConvert)
        return valueToConvert
    except:
        print("GARBAGE")
        return "GARBAGE"


# sqlanydb.register_converter(sqlanydb.DT_VARCHAR, varchar_callback)


def iter_sybase_table(
    tablename: str, db_params, colonnes=None, where=None
) -> namedtuple:
    """Itérateur renvoyant les enregistrements d'une table"""
    links = db_params["links"]
    eng = db_params["eng"]
    dbn = db_params["dbn"]
    uid = db_params["uid"]
    pwd = db_params["pwd"]
    try:
        # connect to the SQL Any database
        connexion_remote_intranet = sqlanydb.connect(
            links=links, eng=eng, dbn=dbn, uid=uid, pwd=pwd
        )
        # initialize the cursor
        cursor = connexion_remote_intranet.cursor()

        # traitement des colonnes sélectionnées
        if colonnes is None:
            select_sql = f"select * from {tablename}"
        else:
            # très important pour les tables qui ont des centaines de colonnes !
            str_colonnes = ",".join(colonnes)
            select_sql = f"select {str_colonnes} from {tablename}"

        # Traitement de la clause where
        if where is None:
            pass
        else:
            select_sql += f" where {where}"

        select_sql += ";"

        try:
            cursor.execute(select_sql)
            # print(f"Success while executing {select_sql}")
        except:
            pass

        # Get a cursor description which contains column names
        desc = cursor.description
        all_colonnes = []
        for i, col in enumerate(range(len(desc))):
            attribut = desc[col][0]
            all_colonnes += [attribut]
            # print(f"{i} {attribut}")

        record_maker = namedtuple(f"{tablename}", all_colonnes)
        # print(f"Colonnes sélectionnées: {all_colonnes}\nrecord_maker:{record_maker}")

        # Fetch all results from the cursor into a sequence,
        # display the values as column name=value pairs,
        # and then close the connection
        try:
            rowset = cursor.fetchall()
        except Exception as e:
            rowset = None
            print(f"pb {e} in cursor.fetchall()")

        if rowset is not None:
            for i, row in enumerate(rowset):
                values = []
                for col in range(len(desc)):
                    attribut = desc[col][0]
                    value = row[col]
                    values += [value]
                record = record_maker._make(values)
                yield record

        cursor.close()
        connexion_remote_intranet.close()
    except Exception as e:
        print(f"Exception:{e} links:'{links}' dbn='{dbn}' on eng='{eng}' uid={uid}")


def print_sybase_records(tablename: str, db_params, colonnes=None, where=None) -> None:
    """Lecture des enregistrements d'une table d'une base SQL Anywhere."""
    liste_records = []
    for i, record in enumerate(
        iter_sybase_table(tablename, db_params, colonnes, where)
    ):
        try:
            print(f"N°{i} {record}")
        except:
            pass


def get_sybase_records(
    tablename: str, db_params, colonnes=None, where=None
) -> List[namedtuple]:
    """Lecture des enregistrements d'une table d'une base SQL Anywhere.
    en mémoire.

    Ne pas l'employer sur de grands volumes comme les fiches de
    temps par exemple.

    """
    liste_records = []
    for i, record in enumerate(
        iter_sybase_table(tablename, db_params, colonnes, where)
    ):
        liste_records += [record]

    return liste_records
