.. index::
   pair: Database ; PostgreSQL
   ! PostgreSQL

.. _postgresql_ref:

=================
**PostgreSQL**
=================


- :ref:`postgresql:postgresql_ref`
