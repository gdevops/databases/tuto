.. index::
   pair: MariaDB ; service

.. _mariadb_service_commands:

===================================
MariaDB service commands
===================================




MariaDB server status
==========================


::

    sudo service mariadb status


::

    ● mariadb.service - MariaDB 10.3.22 database server
       Loaded: loaded (/lib/systemd/system/mariadb.service; enabled; vendor preset: enabled)
       Active: active (running) since Thu 2020-04-30 09:43:41 CEST; 2s ago
         Docs: man:mysqld(8)
               https://mariadb.com/kb/en/library/systemd/
      Process: 7050 ExecStartPre=/usr/bin/install -m 755 -o mysql -g root -d /var/run/mysqld (code=exited, status=0/SUCCESS)
      Process: 7051 ExecStartPre=/bin/sh -c systemctl unset-environment _WSREP_START_POSITION (code=exited, status=0/SUCCESS)
      Process: 7053 ExecStartPre=/bin/sh -c [ ! -e /usr/bin/galera_recovery ] && VAR= ||   VAR=`/usr/bin/galera_recovery`; [ $? -eq 0 ]   && systemctl set-environment _WSREP_START_POSITION=$VAR || exit 1 (code=exite
      Process: 7136 ExecStartPost=/bin/sh -c systemctl unset-environment _WSREP_START_POSITION (code=exited, status=0/SUCCESS)
      Process: 7138 ExecStartPost=/etc/mysql/debian-start (code=exited, status=0/SUCCESS)
     Main PID: 7105 (mysqld)
       Status: "Taking your SQL requests now..."
        Tasks: 31 (limit: 4915)
       Memory: 75.3M
       CGroup: /system.slice/mariadb.service
               └─7105 /usr/sbin/mysqld

    avril 30 09:43:41 uc045 systemd[1]: Starting MariaDB 10.3.22 database server...
    avril 30 09:43:41 uc045 mysqld[7105]: 2020-04-30  9:43:41 0 [Note] /usr/sbin/mysqld (mysqld 10.3.22-MariaDB-0+deb10u1) starting as process 7105 ...
    avril 30 09:43:41 uc045 systemd[1]: Started MariaDB 10.3.22 database server.


.. _restart_mariadb_server:

Restart the MariaDB server
============================

::

    sudo service mariadb restart
