.. index::
   pair: superset ; pinot

.. _superset_pinot:

==============================================================================
Apache **superset** pinot integration
==============================================================================

- https://docs.pinot.apache.org/integrations/superset
