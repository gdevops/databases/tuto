
.. _redis_versions:

===================
Redis versions
===================

.. seealso::

   - https://github.com/antirez/redis/releases

.. toctree::
   :maxdepth: 3


   6.0.1/6.0.1
   6.0.0/6.0.0
   5.0.4/5.0.4
