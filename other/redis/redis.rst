
.. index::
   pair: Redis ; Remote Dictionary Server
   ! Redis

.. _redis:

=================================================================================================
Redis /rɛdɪs/ (Remote DIctionary Server) by Salvatore Sanfilippo (https://x.com/antirez)
=================================================================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Redis
   - https://github.com/antirez/redis
   - https://redis.io/
   - https://x.com/antirez


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
