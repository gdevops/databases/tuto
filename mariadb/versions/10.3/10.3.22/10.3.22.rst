
.. index::
   pair: MariaDB ; 10.3.22 (2020-01-26)

.. _mariadb_10_3_22:

===========================================================
MariaDB 10.3.22 (2020-01-26)
===========================================================

.. seealso::

   - https://mariadb.com/kb/en/mariadb-10322-changelog/
   - https://github.com/MariaDB/server/releases/tag/mariadb-10.3.22
   - https://mariadb.com/kb/en/mariadb-10322-release-notes/


MariaDB 10.3.22 is a Stable (GA) release.
