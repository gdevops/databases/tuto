
.. _superset_installation_docker_compose:

====================================================================
**superset** installation with docker-compose
====================================================================

- https://hub.docker.com/r/apache/superset
- https://superset.apache.org/docs/installation/installing-superset-using-docker-compose


Launch Superset Through Docker Compose
===========================================

Navigate to the folder you created in step 1::

    $ cd superset

Then, run the following command::

    $ docker-compose -f docker-compose-non-dev.yml up

You should see a wall of logging output from the containers being launched
on your machine.

Once this output slows, you should have a running instance of Superset
on your local machine !

.. warning: This will bring up superset in a non-dev mode, changes to
  the codebase will not be reflected.

  If you would like to run superset in dev mode to test local changes,
  simply replace the previous command with: docker-compose up, and wait
  for the superset_node container to finish building the assets.


dev
=====

::

    sudo service postgresql stop
    sudo service postgresql status

    docker-compose  up
    docker-compose  exec superset bash
    docker-compose  down
    docker-compose  logs
    docker-compose  exec superset pip list


non-dev
=========

::

    docker-compose -f docker-compose-non-dev.yml up
    docker-compose -f docker-compose-non-dev.yml down
    docker-compose -f docker-compose-non-dev.yml exec superset bash
    docker-compose -f docker-compose-non-dev.yml exec superset pip list | grep psycopg2
    docker-compose -f docker-compose-non-dev.yml exec superset pip uninstall psycopg2-binary
    docker-compose -f docker-compose-non-dev.yml exec superset pip install psycopg2-binary==2.8.6
    docker-compose -f docker-compose-non-dev.yml restart
    docker-compose -f docker-compose-non-dev.yml exec superset pip list | grep psycopg2

::

    docker-compose -f docker-compose-non-dev.yml exec superset bash
    pip uninstall psycopg2-binary
