
.. _dbeaver_def:

=========================================================
DBeaver description
=========================================================

.. seealso::

   - https://github.com/dbeaver/dbeaver/blob/devel/README.md




Description
===========

Free multi-platform database tool for developers, SQL programmers, database
administrators and analysts.

Supports any database which has JDBC driver (which basically means - ANY database).

EE version also supports non-JDBC datasources (WMI, MongoDB, Cassandra, Redis).

Has a lot of features including metadata editor, SQL editor, rich data editor,
ERD, data export/import/migration, SQL execution plans, etc.

Based on Eclipse platform.

Uses plugins architecture and provides additional functionality for the following
databases: MySQL/MariaDB, PostgreSQL, Greenplum, Oracle, DB2 LUW, Exasol, SQL Server,
Sybase/SAP ASE, SQLite, Firebird, H2, HSQLDB, Derby, Teradata, Vertica, Netezza,
Informix, etc.
