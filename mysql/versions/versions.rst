
.. index::
   pair: Versions ; MySQL


.. _mysql_versions:

================
MySQL versions
================



.. toctree::
   :maxdepth: 3


   8.0.0/8.0.0
