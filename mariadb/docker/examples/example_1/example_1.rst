

.. index::
   pair: docker-compose.yml ; docker

.. _mariadb_docker_example_1:

=========================
MariaDB docker example 1
=========================




Dockerfile
===========

.. literalinclude:: docker-db/Dockerfile
   :linenos:


docker-compose_base.yml
=========================

.. literalinclude:: docker-compose_base.yml
   :linenos:


docker-compose_dev.yml
=========================

.. literalinclude:: docker-compose_base.yml
   :linenos:

Launch the **db** service
==========================

::

    docker-compose -f docker-compose_base.yml -f docker-compose_dev.yml up $1

Exec in db service
====================

::

    docker-compose -f docker-compose_base.yml  -f docker-compose_dev.yml exec db bash


Connect to the database
=========================

::

    mysql -u root -p<password>

::

    Welcome to the MariaDB monitor.  Commands end with ; or \g.
    Your MariaDB connection id is 17
    Server version: 10.3.13-MariaDB-1:10.3.13+maria~bionic mariadb.org binary distribution

    Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

    Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

    MariaDB [(none)]> show databases;
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | mysql              |
    | performance_schema |
    +--------------------+







mysql -u root -pid338
