
.. index::
   pair: import ; mysql


.. _mysql_import:

===================
mysql import
===================

::

    mysql -h db-staging.dmz.id3.lan --user=<username> --password=<password> <database_name> < /root/scripts/output_backup/<database_name>_$(date +%F).sql
