

.. index::
   pair: MariaDB ; FAQ
   pair: Export ; mysqldump
   pair: MariaDB ; mysqldump

.. _mariadb_faq:

===========
MariaDB FAQ
===========




.. _dump_mariadb:
.. _howto_dump_database:

How to dump a Mariab database ?
=================================

.. seealso::

   - https://mariadb.com/kb/en/library/backup-and-restore-overview/#mysqldump

::

    mysqldump -u root -p<password> --dump-date --databases --tz-utc {dbname} > {dbname}.sql


How to delete a database
==========================

::

    drop database <db_name>

::

    MariaDB [(none)]> show databases;
    +--------------------+
    | Database           |
    +--------------------+
    | db_test            |
    | information_schema |
    | mysql              |
    | performance_schema |
    | test               |
    +--------------------+
    5 rows in set (0.00 sec)

::

    MariaDB [(none)]> drop database db_test;
    Query OK, 25 rows affected (0.35 sec)

    MariaDB [(none)]> show databases;                                                                                                                                                                                                            +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | mysql              |
    | performance_schema |
    | test               |
    +--------------------+
    4 rows in set (0.00 sec)
