
.. index::
   pair: SQL Anywhere ; Docker

.. _sqlanywhere_docker:

=================================
SQL Anywhere on Docker
=================================




https://github.com/jaswdr/docker-image-sybase (Ubuntu based)
==============================================================

.. seealso::

   - https://hub.docker.com/r/jaschweder/sybase/~/dockerfile/
   - https://github.com/jaswdr/docker-image-sybase
   - https://hub.docker.com/r/jaschweder/sybase/



https://github.com/nguoianphu/docker-sybase
==============================================

.. seealso::

   - https://github.com/nguoianphu/docker-sybase



https://www.petersap.nl/SybaseWiki/index.php?title=Docker_-_How_to_run_Sybase_ASE_in_a_container#Initial_docker_setup
========================================================================================================================

.. seealso::

   - https://www.petersap.nl/SybaseWiki/index.php?title=Docker_-_How_to_run_Sybase_ASE_in_a_container#Initial_docker_setup
