"""Exemples d'accès aux bases de données "intranet" et "id3semi".

"""

# https://github.com/sqlanywhere/sqlanydb
import sqlanydb

# https://github.com/google/python-fire/blob/master/docs/guide.md
import fire

from tool_sybase import PARAMS_DBGPAO, PARAMS_DBINTRANET


def connexion_remote(params):
    """Connexion à une base sybase"""
    links = params["links"]
    eng = params["eng"]
    dbn = params["dbn"]
    uid = params["uid"]
    pwd = params["pwd"]

    try:
        connexion_remote_intranet = sqlanydb.connect(
            links=links, eng=eng, dbn=dbn, uid=uid, pwd=pwd
        )
        curs = connexion_remote_intranet.cursor()
        curs.execute("select 'Hello, world!'")
        message = curs.fetchone()
        print(f"links:'{links}' dbn='{dbn}' on eng='{eng}' uid={uid} says: {message}")
        curs.close()
        connexion_remote_intranet.close()
    except:
        print(f"Can't connect to the '{dbn}' database")


def test_connexion_remote_intranet():
    """Connexion à la base distante intranet

    Exemple d'appel::

        pipenv run python test_sybase_connect.py test_connexion_remote_intranet

    """
    params = PARAMS_DBINTRANET
    connexion_remote(params)


def test_connexion_remote_id3semi():
    """Connexion à la base distante id3semi

    Exemple d'appel::

        pipenv run python test_sybase_connect.py test_connexion_remote_id3semi

    """
    params = PARAMS_DBGPAO
    connexion_remote(params)


if __name__ == "__main__":
    fire.Fire()
